# RPGCharacters Assignment 1

## Minimum Requirements
Use plain C# to create a console application with the following minimum requirements 
(See Appendix A-C for details):
a) Various character classes having attributes which increase at different rates as the character gains 
levels.
b) Equipment, such as armor and weapons, that characters can equip. The equipped items will alter the 
power of the character, causing it to deal more damage and be able to survive longer. Certain 
characters can equip certain item types.
c) Summary (///) tags for each method you write, explaining what the method does, any exceptions it 
can throw, and what data it returns (if applicable). You do not need to write summary tags for 
overloaded methods.
d) Custom exceptions. There are two custom exceptions you are required to write, as detailed in 
Appendix B.
e) Full test coverage of the functionality. Some testing data is provided, it can be used to complete the 
assignment in a test-driven development manner


## Character classes and attributes

### 1) Introduction and overview
The image shown here is an example of a character select screen in an Action RPG called Diablo 2. 
In the game there are currently four classes that a character/hero can be:
• Mage
• Ranger
• Rogue
• Warrior

Characters in the game have several types of attributes, which represent different aspects of the character. They are 
divided into two groups:
• Primary attributes
• Secondary attributes

Primary attributes are what determine the characters power, as the character levels up their primary attributes 
increase. Items they equip also can increase these primary attributes (this is detailed in Appendix B). Secondary 
attributes affect the characters’ ability to survive, they are calculated from primary attributes. Certain character classes 
are more durable against certain types of damage.

>NOTE: Characters are not required to take damage. The secondary attributes are there to simply add value to 
>attributes that do not affect the power of the character. For the requirements, it will be a simple calculate and 
>display.

### 2) Attributes
The attribute system found in this assignment is based on the traditional Three-Stat System leaning towards a Diablo 3
implementation. Firstly, looking at primary attributes:
• Strength – determines the physical strength of the character.
o Each point of strength increases a Warriors damage by 1%.
• Dexterity – determines the characters ability to attack with speed and nimbleness.
o Each point of dexterity increases a Rangers and Rogues damage by 1%.
• Intelligence – determines the characters affinity with magic.
o Each point of intelligence increases a Mages damage by 1%.
• Vitality – determines how much health a character has overall.
o Each point of vitality increases a character’s health by 10.
As for secondary attributes. As mentioned above, they are calculated based on primary attributes:
• Health – determines how much overall damage a character can take before they die.
o Each point of vitality increases health by 10.
o Health = (Total Vitality) *10
• Armor Rating – determines the physical resistance of the character.
o Each point of Strength or Dexterity adds one armor rating.
o Armor Rating = Total Strength + Total Dexterity
• Elemental Resistance – determines how resistance the character is to magic damage.
o Each point of Intelligence adds one elemental resistance.
o Elemental Resistance = Total Intelligence

Finally, a character can deal Damage. The amount of damage depends on the weapon equipped, the character class, and 
the amount of primary attribute the character has. This calculation is detailed in Appendix B when items are discussed.
It is recommended to make custom types called PrimaryAttribute and SecondaryAttribute to encapsulate the various 
attributes. 
It is also recommended to overload the + operator for these custom types to simplify increasing attributes. This is 
detailed in a demo video.

### 3) Character classes and levelling
It is recommended to have a base abstract Character/Hero class that can be used to encapsulate the functionality. 
Functionality that is different between character classes can be defined as an abstract method in the base class, to be 
implemented in the inheriting class. This is to limit repeated functionality – DRY.
All characters have the following properties:
• Name
• Level
• Base Primary attributes
• Total Primary attributes
• Secondary attributes

>NOTE: There are more properties a character has, those are relating to items and equipment. Those properties 
>will be detailed in Appendix B.

When a character is created, they are provided a name. Every character begins at level 1. There should be a way to 
increase the level of a character.
The separation of base and total primary attributes is for a very simple reason that will help avoid a great deal of 
trouble. It is done this way so the characters primary attributes from itself and its levels are separate from the bonus 
primary attribute from equipment. This is detailed more in Appendix B, but all that needs to be knows now is that it 
simplifies switching equipment.

3.1) Mage attribute gain

A Mage begins at level 1 with:
5 Vitality 1 Strength 1 Dexterity 8 Intelligence

Every time a Mage levels up, they gain:
3 Vitality 1 Strength 1 Dexterity 5 Intelligence

>RECALL: Mages deal increased damage for every point of Intelligence.

3.2) Ranger attribute gain

A Ranger begins at level 1 with:
8 Vitality 1 Strength 7 Dexterity 1 Intelligence

Every time a Ranger levels up, they gain:
2 Vitality 1 Strength 5 Dexterity 1 Intelligence

>RECALL: Rangers deal increased damage for every point of Dexterity.

3.3) Rogue attribute gain

A Rogue begins at level 1 with:
8 Vitality 2 Strength 6 Dexterity 1 Intelligence

Every time a Rogue levels up, they gain:
3 Vitality 1 Strength 4 Dexterity 1 Intelligence

>RECALL: Rogues deal increased damage for every point of Dexterity.

3.4) Warrior attribute gain

A Warrior begins at level 1 with:
10 Vitality 5 Strength 2 Dexterity 1 Intelligence

Every time a Warrior levels up, they gain:
5 Vitality 3 Strength 2 Dexterity 1 Intelligence

>RECALL: Warriors deal increased damage for every point of Strength.Appendix B: Items and equipment 

### 1) Introduction and overview
The game has items which exist. These items can be equipped by characters to increase their power, this is called 
equipping an item. The currently equipped items are called the characters equipment.
There are two types of items which exist:
• Weapons
• Armor
Weapons determine the damage a character can deal, which is then enhanced by the characters attributes. Armor adds 
to the attributes of a character, normally for defense.
Certain characters can only equip specific types of weapons and armor, custom exceptions are used to give proper 
feedback on this.
It is recommended to have a base abstract Item with Weapon and Armor can inherit. These simplifies equipment 
management greatly.
All items have:
• Name
• Required level to equip the item.
• Slot in which the item is equipped.

### 2) Weapons
There are several types of weapons which exist:
• Axes
• Bows
• Daggers
• Hammers
• Staffs
• Swords
• Wands

It is recommended to store these types as a property in the Weapon. An enumerator could be useful here to compose 
your weapon with its type.
Weapons have a base damage, and how many attacks per second can be performed with the weapon. The weapons 
damage per second (DPS) is calculated by multiplying these together.
• DPS = Damage * Attack speed

>NOTE: When calculating the DPS of a character, the weapon DPS is used, not the base damage.

As mentioned before, certain character classes can equip certain weapon types. This is shown below:
• Mages – Staff, Wand
• Rangers – Bow
• Rogues – Dagger, Sword
• Warriors – Axe, Hammer, Sword

If a character tries to equip a weapon they should not be able to, either by it being the wrong type or being too high of a 
level requirement, then a custom InvalidWeaponException should be thrown.

It is recommended to think about how this Weapon check is implemented. Try out some implementations and decide on 
which is best (hint, think about some OO Design here, maybe it could be in the inherited classes with a base abstract 
method to be more extendable). You can use unit testing to see if your refactors break the functionality.

### 3) Armor
There are several types of Armor that exist:
• Cloth
• Leather
• Mail
• Plate
As with Weapons, it is recommended to store this type as property.
Armor has attributes that add to the character’s power. These attributes are the same as the primary attributes and this 
means the PrimaryAttribute custom type can be reused.
Like Weapons, Armor is restricted to certain character classes. This is shown below:
• Mages – Cloth
• Rangers – Leather, Mail
• Rogues – Leather, Mail
• Warriors – Mail, Plate
If a character tries to equip armor they should not be able to, either by it being the wrong type or being too high of a 
level requirement, then a custom InvalidArmorException should be thrown.
Like with weapons, it is recommended to try different implementations of the Armor check and settle on one that is the 
best designed, according to you.

### 4) Equipment
A character can equip any item. An equipped item is stored in the character’s equipment and is used to increase the 
characters power. 
Items can be equipped in one of several slots:
• Head
• Body
• Legs
• Weapon
Armor can be equipped in any non-weapon slot, and weapons can only be equipped in a weapon slot. You can create 
Slot as an enumerator.

>NOTE: You do not have to add a check to see if a weapon has its slot as Weapon, you can assume data will be 
>created properly. Testing data is provided to you to use. 
>It is recommended to store the equipment as a Dictionary<Slot, Item>. This is to ensure you only ever have one item in 
>each slot. You will just replace the Item that corresponds to the Slot. 

>NOTE: You do not need to cater for unequipped items, they just are removed. Ideally, they would go into an 
>inventory, but that is not part of the base requirements.

It is also recommended to have two Equip methods on a character, one with a Weapon as a parameter, and the other 
with Armor. This way there is one public facing Equip method that takes the different types of items.

4.1) Total attributes and calculations
As mentioned before, every character has a base and total primary attribute. When the total is needed you should look 
at what armor is equipped and add all the primary attributes present in those items to the base.
• Total attribute = base + attributes from all equipped armor
This can then be used to determine the character’s DPS
• Character DPS = Weapon DPS * (1 + TotalPrimaryAttribute/100)
Recall, when we speak about primary attribute here, it is the one that increases the damage for the class (Strength for 
Warriors, etc..). Hint: you can see how this will behave differently based on the character class. Consider using 
polymorphism here to aid you.

>NOTE: If a character has no weapon equipped, take their DPS to equal 1.

### 5) Character stats display
All characters need a way to display their statistics to a character sheet. For this example, a simple string generated by 
using a StringBuilder is a good solution. This sheet should show:
• Character name
• Character level
• Strength
• Dexterity 
• Intelligence
• Health
• Armor Rating
• Elemental Resistance
• DPS
The attribute statistics is the total (base + gear bonus).

>NOTE: These values change as the character levels up or equips new items.
