﻿using RPGCharacters.CharacterClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public abstract class Character : IEquippable
    {
        public string Name { get; set; }
        public int Level { get; set; }
        
        private PrimaryAttributes totalPrimaryAttribute = new PrimaryAttributes();
        public PrimaryAttributes TotalPrimaryAttribute
        {
            get { return totalPrimaryAttribute; }
            set { PrimaryAttributes totalPrimaryAttribute = value; }
        }
        public SecondaryAttributes SecondaryAttributes { get; set; } = new SecondaryAttributes();

        public Dictionary<Slot, Armor> EquippedArmor = new Dictionary<Slot, Armor>();
        public Weapon EquippedWeapon { get; set; }

        public abstract void InitializeClassPrimaryAttributes();

        /// <summary>
        /// Starting level is set to 1 and intitalizing empty armor slots for future use.
        /// </summary>
        public Character()
        {
            Level = 1;
            InitializeEmptyArmorSlots();
        }

        /// <summary>
        /// Initializes empty armor slots in EquippedArmor Dictionary with slot as key and value set default as null.
        /// </summary>
        public void InitializeEmptyArmorSlots()
        {
            EquippedArmor.Add(Slot.HEAD_SLOT, null);
            EquippedArmor.Add(Slot.BODY_SLOT, null);
            EquippedArmor.Add(Slot.LEGS_SLOT, null);
        }


        public string PrintCharacterStats()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append($"---------------------------\n");
            stringBuilder.Append($"Character Name: {Name}\n");
            stringBuilder.Append($"Character Level: {Level}\n");
            stringBuilder.Append($"Character Class: {this.GetType().Name}\n");
            stringBuilder.Append($"Strength: {TotalPrimaryAttribute.Strength}\n");
            stringBuilder.Append($"Dexterity: {TotalPrimaryAttribute.Dexterity}\n");
            stringBuilder.Append($"Intelligence: {TotalPrimaryAttribute.Intelligence}\n");
            stringBuilder.Append($"Health: {SecondaryAttributes.Health}\n");
            stringBuilder.Append($"Armor Rating: {SecondaryAttributes.ArmorRating}\n");
            stringBuilder.Append($"ElementalResistance: {SecondaryAttributes.ElementalResistance}\n");
            stringBuilder.Append($"DPS: {CalculateDPS()}");
            stringBuilder.Append($"\n---------------------------");


            return stringBuilder.ToString();
        }
        /// <summary>
        /// Levels up input number of times and calculates Primary and Secondary attributes. If levels is 0 or negative returns ArgumentException.
        /// </summary>
        /// <param name="levels">Number of levels to level up</param>
        public void LevelUp(int levels)
        {
            try
            {
                if (levels < 1)
                {
                    throw new ArgumentException();
                }
                else
                {
                    TotalPrimaryAttribute.LevelUp(this.GetType(), levels);
                    Level += levels;
                    TotalPrimaryAttribute.CalculateSecondaryAttributes(SecondaryAttributes);
                }
            }
            catch (ArgumentException)
            {
                throw new ArgumentException("Error! Cant gain less than 1 level!");
            }
        }

        /// <summary>
        /// Calculates Damage Per Second (DPS) with or without a weapon equipped.
        /// </summary>
        /// <returns>Damage Per Second (DPS)</returns>
        public double CalculateDPS()
        {
            double characterPrimaryBonusAttribute = Convert.ToDouble(GetCharacterIncreasedDamageAttribute());

            if (EquippedWeapon is not null)
            {
                WeaponAttribute weapon = EquippedWeapon.WeaponAttributes;

                return (weapon.Damage * weapon.AttackSpeed) * (1 + (characterPrimaryBonusAttribute / 100));
            }
            else
            {
                return (1 + (characterPrimaryBonusAttribute / 100));
            }
        }

        /// <summary>
        /// Gets the type name of the instance and finds the predetermined bonus attribute.
        /// </summary>
        /// <returns>Character specific bonus attribute. Returns 0 if not found.</returns>
        public int GetCharacterIncreasedDamageAttribute()
        {
            string className = this.GetType().Name;
            switch (className)
            {
                case "Mage":
                    return TotalPrimaryAttribute.Intelligence;
                case "Ranger":
                    return TotalPrimaryAttribute.Dexterity;
                case "Rogue":
                    return TotalPrimaryAttribute.Dexterity;
                case "Warrior":
                    return TotalPrimaryAttribute.Strength;
                default:
                    return 0;
            }
        }

        /// <summary>
        /// Takes an armor and adds it's attributes to the characters Total Primary Attribute.
        /// </summary>
        /// <param name="armor">Armor to add attributes from.</param>
        public void AddArmorAttributesToCharacterStats(Armor armor)
        {
            TotalPrimaryAttribute.Dexterity += armor.PrimaryAttribute.Dexterity;
            TotalPrimaryAttribute.Intelligence += armor.PrimaryAttribute.Intelligence;
            TotalPrimaryAttribute.Strength += armor.PrimaryAttribute.Strength;
            TotalPrimaryAttribute.Vitality += armor.PrimaryAttribute.Vitality;
        }

        #region EquipWeapon with checks
        /// <summary>
        /// Checks if weapon is equippable by checking type against valid weapon types and if the weapon is equippable by ItemLevel. If successful, equips weapon.
        /// </summary>
        /// <param name="weapon">Weapon to equip.</param>
        /// <returns>"New weapon equipped!" if successful. Otherwise It returns appropriate error string.</returns>
        public string EquipWeapon(Weapon weapon)
        {
            if (!IsEquippableWeapon(weapon, ValidWeaponTypes()))
            {
                if (!IsEquippableWeaponType(weapon, ValidWeaponTypes()))
                {
                    return $"A { this.GetType().Name} can't equip a {weapon.ItemName}!";
                }
                else
                {
                    return $"This {this.GetType().Name} is not high enough level to equip {weapon.ItemName} at Itemlevel {weapon.ItemLevel}!";
                }
            }
            else
            {
                EquippedWeapon = weapon;
                return "New weapon equipped!";
            }
        }

        /// <summary>
        /// Finds the Character type name of the current instance and matches it to the class specific equippable weapons list in the database.
        /// </summary>
        /// <returns>A list of weapontypes which is equippable by the current Character type. Returns empty List if not found.</returns>
        public List<WeaponType> ValidWeaponTypes()
        {
            switch (this.GetType().Name)
            {
                case "Mage":
                    return Db.MageEquippableWeapons;
                case "Ranger":
                    return Db.RangerEquippableWeapons;
                case "Rogue":
                    return Db.RogueEquippableWeapons;
                case "Warrior":
                    return Db.WarriorEquippableWeapons;
                default:
                    return new List<WeaponType>() { };
            }
        }

        /// <summary>
        /// Checks if weapon is equippable by type and itemlevel.
        /// </summary>
        /// <param name="weapon">Weapon to equip</param>
        /// <param name="weapontypes">Weapon types that can be equipped.</param>
        /// <returns>True if equippable by type and itemlevel and false if not equippable.</returns>
        public bool IsEquippableWeapon(Weapon weapon, List<WeaponType> weapontypes)
        {
            if (IsEquippableWeaponItemLevel(weapon) && IsEquippableWeaponType(weapon, weapontypes))
            {
                return true;
            }
            else return false;
        }

        /// <summary>
        /// Checks if the weapon type is equippable.
        /// </summary>
        /// <param name="weapon">Weapon to equip.</param>
        /// <param name="weapontypes">Weapon types that can be equipped.</param>
        /// <returns>True if weapon type exists in weapontypes. Otherwise throws InvalidWeaponException.</returns>
        public bool IsEquippableWeaponType(Weapon weapon, List<WeaponType> weapontypes)
        {
            try
            {
                if (weapontypes.Where(validWeaponType => validWeaponType == weapon.WeaponType).ToList().Count == 1) return true;
                else
                {
                    throw new InvalidWeaponException();
                }
            }
            catch (InvalidWeaponException)
            {
                throw new InvalidWeaponException("Invalid weapon! Your cant equip that type of weapon.");
            }
        }

        /// <summary>
        /// Checks if weapon itemlevel is equippable on character by checking its level.
        /// </summary>
        /// <param name="weapon">Weapon to equip.</param>
        /// <returns>True if itemlevel is less or equal to character level. Otherwise returns InvalidWeaponException.</returns>
        public bool IsEquippableWeaponItemLevel(Weapon weapon)
        {
            try
            {
                if (weapon.ItemLevel <= Level) return true;
                else
                {
                    throw new InvalidWeaponException();
                }
            }
            catch (InvalidWeaponException)
            {
                throw new InvalidWeaponException("Invalid weapon! Your level is too low to use this weapon.");
            }
        }

        #endregion

        #region EquipArmor with checks
        /// <summary>
        /// Checks if armor is equippable by checking type against valid armor types and if the armor is equippable by ItemLevel. If successful, equips armor.
        /// </summary>
        /// <param name="armor">Armor to equip.</param>
        /// <returns>"New armor equipped!" if successful. Otherwise It returns appropriate error string.</returns>
        public string EquipArmor(Armor armor)
        {
            if (!IsEquippableArmor(armor, ValidArmorTypes()))
            {
                if (!IsEquippableArmorType(armor, ValidArmorTypes()))
                {
                    return $"A {this.GetType().Name} can't equip a {armor.ItemName}!";
                }
                else
                {
                    return $"This {this.GetType().Name} is not high enough level to equip {armor.ItemName} at Itemlevel {armor.ItemLevel}!";
                }
            }
            else
            {
                EquippedArmor[armor.ItemSlot] = armor;
                AddArmorAttributesToCharacterStats(armor);
                return $"New armor equipped!";
            }
        }
        /// <summary>
        /// Checks if weapon is equippable by type and itemlevel.
        /// </summary>
        /// <param name="armor">Armor to equip.</param>
        /// <param name="armorTypes">Armor types that can be equipped.</param>
        /// <returns>True if equippable by type and itemlevel and false if not equippable.</returns>
        public bool IsEquippableArmor(Armor armor, List<ArmorType> armorTypes)
        {
            if (IsEquippableArmorItemLevel(armor) && IsEquippableArmorType(armor, armorTypes))
            {
                return true;
            }
            else return false;
        }

        /// <summary>
        /// Checks if armor itemlevel is equippable on character by checking its level.
        /// </summary>
        /// <param name="armor">Armor to equip.</param>
        /// <returns>True if itemlevel is less or equal to character level. Otherwise returns InvalidWeaponException.</returns>
        public bool IsEquippableArmorItemLevel(Armor armor)
        {
            try
            {
                if (armor.ItemLevel <= Level) return true;
                else
                {
                    throw new InvalidArmorException();
                }
            }
            catch (InvalidArmorException)
            {
                throw new InvalidArmorException("Invalid armor! Your level is too low to use this weapon.");
            }
        }

        /// <summary>
        /// Checks if armor type is equippable.
        /// </summary>
        /// <param name="armor">Armor to equip.</param>
        /// <param name="armorTypes">Armor types that can be equipped.</param>
        /// <returns>True if armor type exists in armorTypes. Otherwise throws InvalidWeaponException.</returns>
        public bool IsEquippableArmorType(Armor armor, List<ArmorType> armorTypes)
        {
            try
            {
                if (armorTypes.Where(validArmorType => validArmorType == armor.ArmorType).ToList().Count == 1) return true;
                else
                {
                    throw new InvalidArmorException();
                }
            }
            catch (InvalidArmorException)
            {
                throw new InvalidArmorException("Invalid armor! Your cant equip that type of armor.");
            }
        }

        /// <summary>
        /// Finds the Character type name of the current instance and matches it to the class specific equippable armor list in the database.
        /// </summary>
        /// <returns>A list of armortypes which is equippable by the current Character type. Returns empty List if not found.</returns>
        public List<ArmorType> ValidArmorTypes()
        {
            switch (this.GetType().Name)
            {
                case "Mage":
                    return Db.MageEquippableArmor;
                case "Ranger":
                    return Db.RangerEquippableArmor;
                case "Rogue":
                    return Db.RogueEquippableArmor;
                case "Warrior":
                    return Db.WarriorEquippableArmor;
                default:
                    return new List<ArmorType>() { };
            }
        }
        #endregion

    }
}
