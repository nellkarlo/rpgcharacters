﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.CharacterClasses
{
    public class Mage : Character, IEquippable
    {
        /// <summary>
        /// Each time a mage class is created it runs InitializeClassPrimaryAttributes() and calculcates the secondary attributes
        /// </summary>
        public Mage()
        {
            InitializeClassPrimaryAttributes();
            TotalPrimaryAttribute.CalculateSecondaryAttributes(SecondaryAttributes);
        }

        /// <summary>
        /// Initializes the current class's primary attributes with values stored in Db class.
        /// </summary>
        public override void InitializeClassPrimaryAttributes()
        {
            TotalPrimaryAttribute.Dexterity += Db.MageStartPrimaryAttributes.Dexterity;
            TotalPrimaryAttribute.Intelligence += Db.MageStartPrimaryAttributes.Intelligence;
            TotalPrimaryAttribute.Strength += Db.MageStartPrimaryAttributes.Strength;
            TotalPrimaryAttribute.Vitality += Db.MageStartPrimaryAttributes.Vitality;
        }
    }
}
