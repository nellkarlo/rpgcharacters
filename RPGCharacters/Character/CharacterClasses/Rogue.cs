﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.CharacterClasses
{
    public class Rogue : Character
    {
        /// <summary>
        /// Each time a rogue class is created it runs InitializeClassPrimaryAttributes() and calculcates the secondary attributes
        /// </summary>
        public Rogue()
        {
            InitializeClassPrimaryAttributes();
            TotalPrimaryAttribute.CalculateSecondaryAttributes(SecondaryAttributes);
        }
        /// <summary>
        /// Initializes the current class's primary attributes with values stored in Db class.
        /// </summary>
        public override void InitializeClassPrimaryAttributes()
        {
            TotalPrimaryAttribute.Dexterity += Db.RogueStartPrimaryAttributes.Dexterity;
            TotalPrimaryAttribute.Intelligence += Db.RogueStartPrimaryAttributes.Intelligence;
            TotalPrimaryAttribute.Strength += Db.RogueStartPrimaryAttributes.Strength;
            TotalPrimaryAttribute.Vitality += Db.RogueStartPrimaryAttributes.Vitality;
        }
    }
}
