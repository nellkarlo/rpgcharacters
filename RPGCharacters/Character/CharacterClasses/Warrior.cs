﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.CharacterClasses
{
    public class Warrior : Character 
    {
        /// <summary>
        /// Each time a warrior class is created it runs InitializeClassPrimaryAttributes() and calculcates the secondary attributes
        /// </summary>
        public Warrior()
        {
            InitializeClassPrimaryAttributes();
            TotalPrimaryAttribute.CalculateSecondaryAttributes(SecondaryAttributes);
        }
        /// <summary>
        /// Initializes the current class's primary attributes with values stored in Db class.
        /// </summary>
        public override void InitializeClassPrimaryAttributes()
        {
            TotalPrimaryAttribute.Dexterity += Db.WarriorStartPrimaryAttributes.Dexterity;
            TotalPrimaryAttribute.Intelligence += Db.WarriorStartPrimaryAttributes.Intelligence;
            TotalPrimaryAttribute.Strength += Db.WarriorStartPrimaryAttributes.Strength;
            TotalPrimaryAttribute.Vitality += Db.WarriorStartPrimaryAttributes.Vitality;
        }
       
    }
}
