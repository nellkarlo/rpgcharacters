﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public class SecondaryAttributes
    {
        public int Health { get; set; }
        public int ArmorRating { get; set; }
        public int ElementalResistance { get; set; }

        public override bool Equals(object obj)
        {
            return obj is SecondaryAttributes attributes &&
                Health == attributes.Health &&
                ArmorRating == attributes.ArmorRating &&
                ElementalResistance == attributes.ElementalResistance;
        }
    }
}
