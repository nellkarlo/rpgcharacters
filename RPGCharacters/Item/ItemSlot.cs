﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public enum Slot
    {
        HEAD_SLOT,
        BODY_SLOT,
        LEGS_SLOT,
        WEAPON_SLOT
    }
}
