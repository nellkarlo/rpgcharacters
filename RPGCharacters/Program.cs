﻿using RPGCharacters.CharacterClasses;
using System;

namespace RPGCharacters
{
    class Program
    {
        static void Main(string[] args)
        {

            Warrior warrior = new Warrior() { Name = "Brandon Sanderson"};
            Weapon testAxe = new Weapon()
            {
                ItemName = "Common axe",
                ItemLevel = 1,
                ItemSlot = Slot.WEAPON_SLOT,
                WeaponType = WeaponType.WEAPON_AXE,
                WeaponAttributes = new WeaponAttribute() { Damage = 7, AttackSpeed = 1.1 }
            };

            Armor testPlateBody = new Armor()
            {
                ItemName = "Common plate body armor",
                ItemLevel = 1,
                ItemSlot = Slot.BODY_SLOT,
                ArmorType = ArmorType.ARMOR_PLATE,
                PrimaryAttribute = new PrimaryAttributes() { Vitality = 2, Strength = 1 }
            };

            Console.WriteLine(warrior.PrintCharacterStats());

            warrior.EquipWeapon(testAxe);
            warrior.EquipArmor(testPlateBody);
            warrior.LevelUp(1);

            Console.WriteLine(warrior.PrintCharacterStats());

            Console.ReadLine();
            

        }
    }
}
