using RPGCharacters;
using RPGCharacters.CharacterClasses;
using System;
using Xunit;

namespace RPGCharactersTests
{
    public class CharacterAttributesAndLevelTest
    {
        [Fact]
        public void Rogue_OnCharacterCreation_IsLevelOne()
        {
            //Arrange 
            int expected = 1;
            int actual;

            //Act
            Rogue rogue = new Rogue();
            actual = rogue.Level;

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void LevelUp_OnLevelUp_IsLevelTwo()
        {
            //Arrange 
            int expected = 2;
            int actual;
            Mage mage = new Mage();

            //Act
            mage.LevelUp(1);
            actual = mage.Level;

            //Assert
            Assert.Equal(expected, actual);
        }
        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void LevelUp_OnLevelUpNegativeNumber_ThrowsArgumentException(int number)
        {
            //Arrange 
            Mage mage = new Mage();

            //Act & Assert
            Assert.Throws<ArgumentException>(() => mage.LevelUp(number));
        }
        #region Character creation has proper default attributes
        [Fact]
        public void Mage_OnCharacterCreation_HasProperDefaultPrimaryAttributes()
        {
            //Arrange
            Mage mage = new Mage();
            PrimaryAttributes actual = mage.TotalPrimaryAttribute;
            PrimaryAttributes expected= new PrimaryAttributes()
            {
                Dexterity = 1,
                Intelligence = 8,
                Strength = 1,
                Vitality = 5
            };
            

            //Act
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Mage_OnCharacterCreation_HasProperDefaultSecondaryAttributes()
        {
            //Arrange
            Mage mage = new Mage();
            SecondaryAttributes actual = mage.SecondaryAttributes;
            SecondaryAttributes expected= new SecondaryAttributes()
            {
                Health = 50,
                ArmorRating = 2,
                ElementalResistance = 8
            };

            //Act
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Ranger_OnCharacterCreation_HasProperDefaultPrimaryAttributes()
        {
            //Arrange
            Ranger ranger = new Ranger();
            PrimaryAttributes actual = ranger.TotalPrimaryAttribute;
            PrimaryAttributes expected= new PrimaryAttributes()
            {

                Dexterity = 7,
                Intelligence = 1,
                Strength = 1,
                Vitality = 8

            };

            //Act
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Ranger_OnCharacterCreation_HasProperDefaultSecondaryAttributes()
        {
            //Arrange
            Ranger ranger = new Ranger();
            SecondaryAttributes actual = ranger.SecondaryAttributes;
            
            SecondaryAttributes expected= new SecondaryAttributes()
            {
                Health = 80,
                ArmorRating = 8,
                ElementalResistance = 1
            };

            //Act
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Rogue_OnCharacterCreation_HasProperDefaultPrimaryAttributes()
        {
            //Arrange
            Rogue rogue = new Rogue();
            PrimaryAttributes actual = rogue.TotalPrimaryAttribute;
            PrimaryAttributes expected= new PrimaryAttributes()
            {

                Dexterity = 6,
                Intelligence = 1,
                Strength = 2,
                Vitality = 8

            };
           
            //Act
            //Assert
            Assert.Equal(expected, actual);

        }
        [Fact]
        public void Rogue_OnCharacterCreation_HasProperDefaultSecondaryAttributes()
        {
            //Arrange
            Rogue rogue = new Rogue();
            SecondaryAttributes actual = rogue.SecondaryAttributes;
            SecondaryAttributes expected= new SecondaryAttributes()
            {
                Health = 80,
                ArmorRating = 8,
                ElementalResistance = 1
            };

            //Act
            //Assert
            Assert.Equal(expected, actual);

        }
        [Fact]
        public void Warrior_OnCharacterCreation_HasProperDefaultPrimaryAttributes()
        {
            //Arrange
            Warrior warrior = new Warrior();
            PrimaryAttributes actual = warrior.TotalPrimaryAttribute;
            PrimaryAttributes expected= new PrimaryAttributes()
            {
                Dexterity = 2,
                Intelligence = 1,
                Strength = 5,
                Vitality = 10
            };

            //Act
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Warrior_OnCharacterCreation_HasProperDefaultSecondaryAttributes()
        {
            //Arrange
            Warrior warrior = new Warrior();
            SecondaryAttributes actual = warrior.SecondaryAttributes;
            SecondaryAttributes expected= new SecondaryAttributes()
            {
                Health = 100,
                ArmorRating = 7,
                ElementalResistance = 1
            };

            //Act
            //Assert
            Assert.Equal(expected, actual);
        }
        #endregion

        #region Character LevelUp has proper attributes 
        [Fact]
        public void MageLevelUp_OnLevelUpOneLevel_HasProperPrimaryAttributes()
        {
            //Arrange
            Mage mage = new Mage();
            PrimaryAttributes actual = mage.TotalPrimaryAttribute;
            PrimaryAttributes expected= new PrimaryAttributes()
            {
                Dexterity = 2,
                Intelligence = 13,
                Strength = 2,
                Vitality = 8
            };

            //Act
            mage.LevelUp(1);

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void MageLevelUp_OnLevelUpOneLevel_HasProperSecondaryAttributes()
        {
            //Arrange
            Mage mage = new Mage();
            SecondaryAttributes actual = mage.SecondaryAttributes;
            SecondaryAttributes expected= new SecondaryAttributes()
            {
                Health = 80,
                ArmorRating = 4,
                ElementalResistance = 13
            };

            //Act
            mage.LevelUp(1);

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void RangerLevelUp_OnLevelUpOneLevel_HasProperPrimaryAttributes()
        {
            //Arrange
            Ranger ranger = new Ranger();
            PrimaryAttributes actual = ranger.TotalPrimaryAttribute;
            PrimaryAttributes expected= new PrimaryAttributes()
            {
                Dexterity = 12,
                Intelligence = 2,
                Strength = 2,
                Vitality = 10
            };
          
            //Act
            ranger.LevelUp(1);

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void RangerLevelUp_OnLevelUpOneLevel_HasProperSecondaryAttributes()
        {
            //Arrange
            Ranger ranger = new Ranger();
            SecondaryAttributes actual = ranger.SecondaryAttributes;
            SecondaryAttributes expected= new SecondaryAttributes()
            {
                Health = 100,
                ArmorRating = 14,
                ElementalResistance = 2
            };

            //Act
            ranger.LevelUp(1);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void RogueLevelUp_OnLevelUpOneLevel_HasProperPrimaryAttributes()
        {
            //Arrange
            Rogue rogue = new Rogue();
            PrimaryAttributes actual = rogue.TotalPrimaryAttribute;
            PrimaryAttributes expected= new PrimaryAttributes()
            {
                Dexterity = 10,
                Intelligence = 2,
                Strength = 3,
                Vitality = 11
            };
            

            //Act
            rogue.LevelUp(1);

            //Assert
            Assert.Equal(expected,actual);
        }
        [Fact]
        public void RogueLevelUp_OnLevelUpOneLevel_HasProperSecondaryAttributes()
        {
            //Arrange
            Rogue rogue = new Rogue();
            SecondaryAttributes actual = rogue.SecondaryAttributes;
            SecondaryAttributes expected= new SecondaryAttributes()
            {
                Health = 110,
                ArmorRating = 13,
                ElementalResistance = 2
            };

            //Act
            rogue.LevelUp(1);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void WarriorLevelUp_OnLevelUpOneLevel_HasProperPrimaryAttributes()
        {
            //Arrange
            Warrior warrior = new Warrior();
            PrimaryAttributes actual = warrior.TotalPrimaryAttribute;
            PrimaryAttributes expected = new PrimaryAttributes()
            {
                Dexterity = 4,
                Intelligence = 2,
                Strength = 8,
                Vitality = 15
            };
            

            //Act
            warrior.LevelUp(1);

            //Assert
            Assert.Equal(expected, actual);

        }
        [Fact]
        public void WarriorLevelUp_OnLevelUpOneLevel_HasProperSecondaryAttributes()
        {
            //Arrange
            Warrior warrior = new Warrior();
            SecondaryAttributes actual = warrior.SecondaryAttributes;
            SecondaryAttributes expected= new SecondaryAttributes()
            {
                Health = 150,
                ArmorRating = 12,
                ElementalResistance = 2
            };

            //Act
            warrior.LevelUp(1);

            //Assert
            Assert.Equal(expected, actual);

        }
        #endregion

        [Fact]
        public void CalculateSecondaryAttributes_OnLevelUpOneLevel_ProperSecondaryAttributes()
        {
            //Arrange
            Warrior warrior = new Warrior();
            SecondaryAttributes actual = warrior.SecondaryAttributes;

            SecondaryAttributes expected = new SecondaryAttributes()
            {
                Health = 150,
                ArmorRating = 12,
                ElementalResistance = 2
            };
            
            //Act
            warrior.LevelUp(1);

            //Assert
            Assert.Equal(expected, actual);
           
        }
    }
}
