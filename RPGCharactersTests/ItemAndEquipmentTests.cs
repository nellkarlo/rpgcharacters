﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPGCharacters;
using RPGCharacters.CharacterClasses;
using Xunit;

namespace RPGCharactersTests
{
    public class ItemAndEquipmentTests
    {
        [Fact]
        public void EquipWeapon_OnWrongWeaponItemLevelEquip_ThrowInvalidWeaponException()
        {
            //Arrange
            Warrior warrior = new Warrior();

            Weapon testAxe = new Weapon()
            {
                ItemName = "Common axe",
                ItemLevel = 2,
                ItemSlot = Slot.WEAPON_SLOT,
                WeaponType = WeaponType.WEAPON_AXE,
                WeaponAttributes = new WeaponAttribute() { Damage = 7, AttackSpeed = 1.1 }
            };

            //Act & Assert
            Assert.Throws<InvalidWeaponException>(() => warrior.EquipWeapon(testAxe));
        }
        [Fact]
        public void EquipArmor_OnWrongArmorItemLevelEquip_ThrowInvalidArmorException()
        {
            //Arrange
            Warrior warrior = new Warrior();

            Armor testPlateBody = new Armor()
            {
                ItemName = "Common plate body armor",
                ItemLevel = 2,
                ItemSlot = Slot.BODY_SLOT,
                ArmorType = ArmorType.ARMOR_PLATE,
                PrimaryAttribute = new PrimaryAttributes() { Vitality = 2, Strength = 1 }
            };

            //Act & Assert
            Assert.Throws<InvalidArmorException>(() => warrior.EquipArmor(testPlateBody));
        }
        [Fact]
        public void EquipWeapon_OnWrongWeaponTypeEquip_ThrowInvalidWeaponException()
        {
            //Arrange
            Warrior warrior = new Warrior();

            Weapon testBow = new Weapon()
            {
                ItemName = "Common bow",
                ItemLevel = 1,
                ItemSlot = Slot.WEAPON_SLOT,
                WeaponType = WeaponType.WEAPON_BOW,
                WeaponAttributes = new WeaponAttribute() { Damage = 12, AttackSpeed = 0.8 }
            };

            //Act & Assert
            Assert.Throws<InvalidWeaponException>(() => warrior.EquipWeapon(testBow));
        }
        [Fact]
        public void EquipArmor_OnWrongArmorTypeEquip_ThrowInvalidArmorException()
        {
            //Arrange
            Warrior warrior = new Warrior();

            Armor testClothHead = new Armor()
            {
                ItemName = "Common cloth head armor",
                ItemLevel = 1,
                ItemSlot = Slot.HEAD_SLOT,
                ArmorType = ArmorType.ARMOR_CLOTH,
                PrimaryAttribute = new PrimaryAttributes() { Vitality = 1, Intelligence = 5 }
            };

            //Act & Assert
            Assert.Throws<InvalidArmorException>(() => warrior.EquipArmor(testClothHead));
        }
        [Fact]
        public void EquipWeapon_OnValidWeaponEquip_ReturnsStringNewWeaponEquipped()
        {
            //Arrange
            Rogue rogue = new Rogue();

            Weapon testDagger = new Weapon()
            {
                ItemName = "Common dagger",
                ItemLevel = 1,
                ItemSlot = Slot.WEAPON_SLOT,
                WeaponType = WeaponType.WEAPON_DAGGER,
                WeaponAttributes = new WeaponAttribute() { Damage = 12, AttackSpeed = 0.8 }
            };
            string expected = "New weapon equipped!";
            string actual;

            //Act
            actual = rogue.EquipWeapon(testDagger);

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void EquipArmor_OnValidArmorEquip_ReturnsStringNewArmorEquipped()
        {
            //Arrange
            Rogue rogue = new Rogue();

            Armor testLeatherBody = new Armor()
            {
                ItemName = "Common leather armor",
                ItemLevel = 1,
                ItemSlot = Slot.BODY_SLOT,
                ArmorType = ArmorType.ARMOR_LEATHER,
                PrimaryAttribute = new PrimaryAttributes() { Vitality = 2, Strength = 1 }
            };
            string expected = "New armor equipped!";
            string actual;

            //Act
            actual = rogue.EquipArmor(testLeatherBody);

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void WarriorCalculateDPS_NoWeaponEquipped_ExpectedDPS()
        {
            //Arrange
            Warrior warrior = new Warrior();
            double expected = 1.0 * (1.0 + (5.0 / 100.0));
            double actual;

            //Act
            actual = warrior.CalculateDPS();

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void WarriorCalculateDPS_AxeEquipped_ExpectedDPS()
        {
            //Arrange
            Warrior warrior = new Warrior();
            Weapon testAxe = new Weapon()
            {
                ItemName = "Common axe",
                ItemLevel = 1,
                ItemSlot = Slot.WEAPON_SLOT,
                WeaponType = WeaponType.WEAPON_AXE,
                WeaponAttributes = new WeaponAttribute() { Damage = 7, AttackSpeed = 1.1 }
            };

            double expected = (7.0 * 1.1) * (1.0 + (5.0 / 100.0));
            double actual;

            //Act
            warrior.EquipWeapon(testAxe);
            actual = warrior.CalculateDPS();

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void WarriorCalculateDPS_AxeAndArmorEquipped_ExpectedDPS()
        {
            //Arrange
            Warrior warrior = new Warrior();
            Weapon testAxe = new Weapon()
            {
                ItemName = "Common axe",
                ItemLevel = 1,
                ItemSlot = Slot.WEAPON_SLOT,
                WeaponType = WeaponType.WEAPON_AXE,
                WeaponAttributes = new WeaponAttribute() { Damage = 7, AttackSpeed = 1.1 }
            };

            Armor testPlateBody = new Armor()
            {
                ItemName = "Common plate body armor",
                ItemLevel = 1,
                ItemSlot = Slot.BODY_SLOT,
                ArmorType = ArmorType.ARMOR_PLATE,
                PrimaryAttribute = new PrimaryAttributes() { Vitality = 2, Strength = 1 }
            };

            //havent implemented Math.Round
            double expected = (7.0 * 1.1) * (1.0 + (6.0 / 100.0));
            double actual;

            //Act
            warrior.EquipWeapon(testAxe);
            warrior.EquipArmor(testPlateBody);
            actual = warrior.CalculateDPS();

            //Assert
            Assert.Equal(expected, actual);
        }
    }
}
